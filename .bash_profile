if [ $TERM == 'xterm' ]; then
    export TERM=xterm-256color
fi

alias gst='git status'
alias gd='git diff --ignore-space-change'
alias gco='git checkout'
alias gb='git branch'
alias gp='git pull'

# with git configured to catch typos with the arguments, alias some typos to 'git'
alias gi='git'
alias gitp='git'

alias l='ls -hlABF --color'
alias ber='bundle exec rake'
alias tmux='TERM=xterm-256color tmux'
alias ber='bundle exec rake'

# for edb-model
export DATABASE_ENV=mbrandt

sshAgentPid=$(ps -ef | grep ssh-agent | grep -v grep | awk '{print $2}')

if [ -z "$sshAgentPid" ]; then
    eval `ssh-agent -s`
    ssh-add ~/.ssh/id_rsa &>/dev/null
fi

cd () {
    if [ "$1" == "..." ]; then
	while [ -z "$(ls -a | grep '^\.git$')" ]; do
	    command cd ..
	done
    else
	command cd $1
    fi
}
